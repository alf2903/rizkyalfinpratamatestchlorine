<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Exception;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::query();

        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $products->where('name', 'like', '%' . $searchTerm . '%');
        }

        $products = $products->paginate(10);

        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(StoreProductRequest $request)
    {
        try {
            Product::create($request->validated());
            return redirect()->route('products.index')->with('success', 'Product Berhasil di Tambahkan.');
        } catch (Exception $e) {
            return redirect()->route('products.index')->with('error', 'Terjadi kesalahan saat menambahkan produk.');
        }
    }

    public function show(string $id)
    {
        //
    }

    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            $isPublish = $request->has('is_publish') ? true : false;

            $product->update([
                'name' => $request->name,
                'description' => $request->description,
                'stock' => $request->stock,
                'unit' => $request->unit,
                'is_publish' => $isPublish,
            ]);

            return redirect()->route('products.index')->with('success', 'Product Berhasil di Edit.');
        } catch (Exception $e) {
            return redirect()->route('products.index')->with('error', 'Terjadi kesalahan saat mengedit produk.');
        }
    }

    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return redirect()->route('products.index')->with('success', 'Product Berhasil di Hapus.');
        } catch (Exception $e) {
            return redirect()->route('products.index')->with('error', 'Terjadi kesalahan saat menghapus produk.');
        }
    }
}
